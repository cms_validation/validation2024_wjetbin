# validation2024_Wjetbin
Compare the process proton proton --> W --> lepton + neutrino + Nj between Madgraph v2_9_18 and v3_5_2 for both LO and NLO, here N = 0, 1, 2 (jet-bin sample).

Samples:
LNu_LO_0j, 
LNu_LO_1j, 
LNu_LO_2j, 
LNu_NLO_0j, 
LNu_NLO_1j, 
LNu_NLO_2j

Slides: 
[link](https://indico.cern.ch/event/1453257/contributions/6117408/attachments/2921883/5130565/validation_zhijun_20240909_v1.1.pdf)

## Generate the gridpack
The packgage [genproductions_master](https://github.com/cms-sw/genproductions/tree/master) and [genproductions_mg352](https://github.com/cms-sw/genproductions/tree/mg352) are using to generate gridpacks of MadGraph v2 and MadGraph v3, the input cards can be found in the dir card/.

code (ref: [click](https://twiki.cern.ch/twiki/bin/viewauth/CMS/QuickGuideMadGraph5aMCatNLO)) (taking v2 and LNu_LO_0j as the example, and cloning the genproductions packgage by yourself):
```
cd genproductions_master/bin/MadGraph5_aMCatNLO
./gridpack_generation.sh LNu_LO_0j card/LNu_LO_0j
```

## Generate the yoda file, calculate the XS and plot
The packgage [genValidation from Jie Xiao](https://gitlab.cern.ch/jixiao/genValidation
) is using to generate the yoda files, calculate the XS and plot.

The gridpack (obtained from the previous step) and genFragment will be the input.
The corresponding genFragment used for different samples are shown below.

LNu_LO_0,1,2j: genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max2j_LHE_pythia8_cff.py

LNu_NLO_0,1,2j: genFragment/Hadronizer_TuneCP5_13TeV_aMCatNLO_FXFX_5f_max2j_LHE_pythia8_cff.py

Cloning the genValidation packgage by yourself, and taking LNu_LO_0j as the example below.

### Generate the yoda file
code:
```
cd genValidation
source setup.sh

./genval-run \
-g gridpack/v2_9_18/LNu_LO_0j_slc7_amd64_gcc10_CMSSW_12_4_8_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max0j_LHE_pythia8_cff.py \
-n 10000 -j 100 -d LNu_LO_0j_v2 -q cmsconnect -m madgraph -b 13600 -a W

./genval-run \
-g gridpack/v3_5_2/LNu_LO_0j_slc7_amd64_gcc10_CMSSW_12_4_8_tarball.tar.xz \
-f genFragment/Hadronizer_TuneCP5_13TeV_MLM_5f_max0j_LHE_pythia8_cff.py \
-n 10000 -j 100 -d LNu_LO_0j_v3 -q cmsconnect -m madgraph -b 13600 -a W
```

### Calculate the XS
```
./genval-xsec -d LNu_LO_0j_v2 -j 0
./genval-xsec -d LNu_LO_0j_v3 -j 0
```

### Plot
Merge the yoda file:
```
./genval-grid LNu_LO_0j_v2
./genval-grid LNu_LO_0j_v3
```

Uncompress the yoda.gz file:
```
gzip -d LNu_LO_0j_v*/merged.yoda.gz
```

plot:
(For myself, I need to login in lxplus9, source setup.sh and set the enviroment of CMSSW_13_3_0, and then I can use the rivet-mkhtml to plot)
```
rivet-mkhtml --errs --no-weights LNu_LO_0j_v2/merged.yoda:Title="LNu_LO_0j(v2_9_18)" LNu_LO_0j_v3/merged.yoda:Title="LNu_LO_0j(v3_5_2)"  -n 8 -o output_LNu_LO_0j
```

The plot result can be found in the dir plot/

How to update the plot (set Xmin, Xmin, Rebin...)? Modify the code update_plot.C, and do
```
root -l update_plot.C
```
